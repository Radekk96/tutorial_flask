from flask import Flask, render_template, url_for
app = Flask(__name__)

posts = [
    {
        'author': 'Radek Kania',
        'title': 'Blog Post 1',
        'content': 'Życie z Darią...',
        'date_posted': 'May 10, 2019'
    },
    {
        'author': 'Daria Bilska',
        'title': 'Blog Post 2',
        'content': 'O jak mi źle.',
        'date_posted': 'July 29, 2018'
    }
]


@app.route("/")
@app.route("/home")
def home():
    return render_template('home.html', posts=posts)


@app.route("/about")
def about():
    return render_template('about.html', title='About')


if __name__ == '__main__':
    app.run(debug=True)